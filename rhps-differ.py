import sys,os,codecs,re,zipfile,difflib,time
from bs4 import BeautifulSoup
from pprint import pprint


#dir_path = ''#os.path.dirname(sys.argv[1]) # should not be global

USAGE_MSG = "Usage: python ./speller.py PATH_TO_EPUB_1 PATH_TO_EPUB_2"

class Epub:
	#constructor does some basic error checking
	def __init__(self, sourcepath=None):
		if sourcepath and os.path.exists(sourcepath) and os.path.isfile(sourcepath) and sourcepath.lower().endswith(".epub"):
			self.sourcepath = sourcepath
			# new model of folder processing
			self.myZipIn = zipfile.ZipFile(self.sourcepath, 'r')
		else:
			#print("error instantiating file " + sourcepath)
			#sys.exit(-1)
			raise Exception("\n\nerror instantiating file " + sourcepath)
	def rezip(self):
		tmp_dir = '.zzz_'+str(time.time())
		#print tmp_dir
		# makes temp working folder in rhps-differ.py folder for unzip-rezip operation, replaces orig epub, deletes tmp folder; returns new epub obj based on rezipped file
		os.system("/usr/bin/ditto -xk  " + self.sourcepath+ " "+tmp_dir)
		os.system('cd '+tmp_dir+'; zip -0X new.epub mimetype > /dev/null 2>&1 ; zip -Xr9D new.epub * -x .DS_Store > /dev/null 2>&1 ; mv new.epub '+self.sourcepath+';cd ..;rm -rf '+tmp_dir)
		return Epub(self.sourcepath)
		### ? for some reason must use shell script zip/unzip instead of Python as per Next Reads ?

	
	def produceSoup(self):
		#print dir(self.myZipIn)
		fullHTML = "<!DOCTYPE html>\n<html>\n<head></head>\n<body>\n"
		soupText = ""
		for zipInfo in self.myZipIn.infolist():
			orig_filename =  zipInfo.orig_filename
			if orig_filename.lower().endswith(".html") or orig_filename.lower().endswith(".htm") or orig_filename.lower().endswith(".xhtml"):
				#print orig_filename
				data = self.myZipIn.read(orig_filename)
				#print data
				aFileBody = data[data.find("<body")+6 : data.rfind("</body")+0] # DO MORE ERROR CHECKING ON THIS SUBSTRING ARITHMETIC; substring start location is <body instead of <body> in case body tag has attributes
				###soup = BeautifulSoup( aFileBody ,  "html.parser" )
				#print soup.get_text()
				###soupText += soup.get_text()
				#print aFileBody
				#print "__________________________________"
				fullHTML += aFileBody +"<hr/>"
		#print "__________________________________"
		fullHTML += "\n<script src = 'jquery-2.1.1.min.js'></script>\n<script src = 'speller.js'></script>\n</body>\n</html>"				
		if len(re.findall("</p><p", fullHTML)) > 0: #fullHTML.find("</p><p")
			fullHTML = fullHTML.replace("</p><p","</p> <p") # prevents false positives from showing up in spellcheck due to whitespace being stripped by soup.get_text()
		#print fullHTML
		self.soup = BeautifulSoup( fullHTML ,  "html.parser" )
		return self.soup


####################################### end Epub class #####################

# Takes an EPUB from the command line as a parameter (inputFilePath).
# Creates EPUB class instance. 
# Creates Beautiful Soup object of EPUB text via produceSoup().
# Returns Beautiful Soup object of EPUB TEXT (myEpub.soup) and absolute path of epub (myEpub.sourcepath)
def processInputFile(inputFilePath):
	try:
		myEpub = Epub(inputFilePath)
		myEpub = myEpub.rezip() #fix weird discrepancies in how epubs may have been zipped up
		myEpub.produceSoup()
		return myEpub.soup, myEpub.sourcepath
	except Exception as e:
		print e

# Takes EBPU1's soup object (epub1), EPUB1's absolute path (epub1_sourcepath), EPUB2's soup object (epub2), and EPUB2's absolute path (epub1_sourcepath).
# Creates difflib HtmlDiff utility (htmldiff).
# Gets the text from each soup object (epub1_text, epub2_text).	
# Creates an HTML table that shows areas of difference in the text from each soup file.
# Prints HTML table to an HTML file in the same directory as the first EPUB called "diff_results.html".	
def compareThoseEPUBs(epub1, epub1_sourcepath, epub2, epub2_sourcepath,dir_path):
	htmldiff = difflib.HtmlDiff(wrapcolumn=40)
	changedLines = []

	epub1_text = epub1.get_text()
	epub2_text = epub2.get_text()
	
	epub1_name = os.path.basename(epub1_sourcepath)
	epub2_name = os.path.basename(epub2_sourcepath)
	
	epub1_lines = epub1_text.splitlines()
	#print len(epub1_lines)
	epub2_lines = epub2_text.splitlines()
	#print len(epub2_lines)
	epub1_lines[:] = (value for value in epub1_lines if value != '')#epub1_lines.remove('')
	epub2_lines[:] = (value for value in epub2_lines if value != '')#epub1_lines.remove('')
	#print epub1_lines.count('')
	#print epub2_lines.count('')
	#print len(epub1_lines)
	#print len(epub2_lines)
	#strip empty lines to ease comparison
	
	html_diff = htmldiff.make_file(epub1_lines, epub2_lines, fromdesc=epub1_name, todesc=epub2_name, context=True, numlines=0)
	#HACK ; find better way to do this #html_diff = html_diff.replace('<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />','<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />')
	html_diff = html_diff.replace('<meta http-equiv="Content-Type"','<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />')
	html_diff = html_diff.replace('content="text/html; charset=ISO-8859-1" />','')


	outputFilename = dir_path + "/diff_results.html"
	outputSoupFile = codecs.open(outputFilename, "wb", "utf-8")
	outputSoupFile.write(html_diff)
	outputSoupFile.close()

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print(USAGE_MSG)
	#if len(sys.argv) == 2:
	#	print("add another EPUB!")
	else: #if len(sys.argv) == 3:
	   dir_path = os.path.dirname(sys.argv[1])
	   try:
		   soup1, soup1_sourcepath = processInputFile(sys.argv[1])
		   soup2, soup2_sourcepath = processInputFile(sys.argv[2])
		   compareThoseEPUBs(soup1, soup1_sourcepath, soup2, soup2_sourcepath,dir_path)
	   except Exception as e:
		   print e
else:
	print "being imported"