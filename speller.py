import sys,os,codecs,re,zipfile
from bs4 import BeautifulSoup
#import xml.dom.minidom as minidom


USAGE_MSG = "Usage: python ./speller.py PATH_TO_EPUB / PATH_TO_UNZIPPED_EPUB"

class Epub:
	#constructor does some basic error checking
	def __init__(self, sourcepath=None):
		if sourcepath and os.path.exists(sourcepath) and os.path.isfile(sourcepath) and sourcepath.lower().endswith(".epub"):
			self.sourcepath = sourcepath
			# new model of folder processing
			self.myZipIn = zipfile.ZipFile(self.sourcepath, 'r')
		else:
			#print("error instantiating file " + sourcepath)
			#sys.exit(-1)
			raise Exception("\n\nerror instantiating file " + sourcepath)

	def produceSpellingFile(self):
		#print dir(self.myZipIn)
		fullHTML = "<!DOCTYPE html>\n<html>\n<head></head>\n<body>\n"
		soupText = ""
		for zipInfo in self.myZipIn.infolist():
			orig_filename =  zipInfo.orig_filename
			if orig_filename.lower().endswith(".html") or orig_filename.lower().endswith(".htm") or orig_filename.lower().endswith(".xhtml"):
				#print orig_filename
				data = self.myZipIn.read(orig_filename)
				#print data
				aFileBody = data[data.find("<body")+6 : data.rfind("</body")+0] # DO MORE ERROR CHECKING ON THIS SUBSTRING ARITHMETIC; substring start location is <body instead of <body> in case body tag has attributes
				###soup = BeautifulSoup( aFileBody ,  "html.parser" )
				#print soup.get_text()
				###soupText += soup.get_text()
				#print aFileBody
				#print "__________________________________"
				fullHTML += aFileBody +"<hr/>"
		fullHTML += "\n<script src = 'jquery-2.1.1.min.js'></script>\n<script src = 'speller.js'></script>\n</body>\n</html>"				
		if len(re.findall("</p><p", fullHTML)) > 0: #fullHTML.find("</p><p")
			fullHTML = fullHTML.replace("</p><p","</p> <p") # prevents false positives from showing up in spellcheck due to whitespace being stripped by soup.get_text()
		#print fullHTML
		soup = BeautifulSoup( fullHTML ,  "html.parser" )
		print soup
		soupText += soup.get_text()

		# the following block pertains to the deprecated jQuery approach
		#print self.sourcepath[self.sourcepath.rfind("/")+1 : self.sourcepath.rfind(".")+0]+".htm"
		####outputFilename = self.sourcepath[self.sourcepath.rfind("/")+1 : self.sourcepath.rfind(".")+0]+".htm"
		####outputHtmlFile = codecs.open(outputFilename, "wb", "utf-8")
		####outputHtmlFile.write(fullHTML)
		####outputHtmlFile.close()

		#print soupText #unicode parse error when output to Terminal, not to file
		#outputFilename = self.sourcepath[self.sourcepath.rfind("/")+1 : self.sourcepath.rfind(".")+0]+".txt"
		outputFilename = self.sourcepath[ 0 : self.sourcepath.rfind(".")+0]+".txt"
		outputSoupFile = codecs.open(outputFilename, "wb", "utf-8")
		outputSoupFile.write(soupText)
		outputSoupFile.close()

####################################### end Epub class #####################

def processInputFile(inputFilePath):
	try:
		myEpub = Epub(inputFilePath)
		myEpub.produceSpellingFile()
	except Exception as e:
		print e

def	processInputFolder(inputFolderPath):
	print "Processing folder " + inputFolderPath
	for file in os.listdir(inputFolderPath):
		aFile =  os.path.join(inputFolderPath,file)
		if aFile.lower().endswith(".epub"):
			print "Processing " + aFile
			processInputFile(aFile)

if __name__ == "__main__":
	#print "main"
	if len(sys.argv) == 2:
		#print sys.argv[1]
		if os.path.exists(sys.argv[1]):
			#print "exists"#and :
			if os.path.isdir(sys.argv[1]):
				#print "assume unzipped epub"
				processInputFolder(sys.argv[1])
			else:
				#print "assume zipped epub"
				processInputFile(sys.argv[1])
		else:
			print sys.argv[1] + " is not a valid input"
			print USAGE_MSG
	else:
		print USAGE_MSG
else:
	print "being imported"