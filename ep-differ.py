import sys,os,zipfile,difflib

class Epub:
	#constructor does some basic error checking
	def __init__(self, sourcepath):
		#self.isbn = ""
		# basic file checks on constructor argument
		if os.path.exists(sourcepath) and os.path.isfile(sourcepath) and sourcepath.endswith(".epub"):
			self.sourcepath = sourcepath
		else:
			self.sourcepath = "NOPE"
		
	def getPages(self):
		zipIn = zipfile.ZipFile(self.sourcepath, 'r')
		self.allTheFiles = zipIn.infolist()
		return self.allTheFiles
		
	def diffTheFiles(self, first_file, second_file, otherEPUB, differ):
		zipIn_first = zipfile.ZipFile(self.sourcepath, 'r')
		zipIn_second = zipfile.ZipFile(otherEPUB.sourcepath, 'r')
		
		#Get the code inside the file.
		first_xml = zipIn_first.read(first_file)
		second_xml = zipIn_second.read(second_file)
		
		#Check to see if there's any difference between the 2 files
		s = difflib.SequenceMatcher(None,first_xml,second_xml)
		boom = s.ratio()
		
		#If there's a difference, run the diff.
		if boom < 1:
			headTitleDoesntCount = 1
			encodedFileName = first_file.encode('ascii','ignore')
			result = list(differ.compare(first_xml.splitlines(),second_xml.splitlines()))
			for line in result:
				if line.startswith("+"):
					if (line.startswith("+ <head><title>") or line.startswith("+       <text>") or line.startswith("+       <dc:title>") or line.startswith('+       <meta property="dcterms:modified">')) and headTitleDoesntCount == 1:
						headTitleDoesntCount = 0
					else:
						headTitleDoesntCount = 2
					#print("ahhhh")
			if headTitleDoesntCount == 2:
				s = '\n'
				#print("--------------------------------- \n\n What follows is a diff of " + encodedFileName + " \n\n" + s.join(result))
				return encodedFileName
		
		#print(diff.getInfo())
			
		
	def compareThoseEPUBs(self, otherEPUB):
		diff = difflib.Differ()
		changedFiles = []
		#matchCheck = []
		#match = 0
		for file in self.allTheFiles:
			match = 0
			for otherfile in otherEPUB.allTheFiles:
				if file.filename == otherfile.filename:
					match = 1
					extension = os.path.splitext(file.filename)[1]
					#if (extension == ".ncx"):
					if (extension == ".xhtml") or (extension == ".htm") or (extension == ".html") or (extension == ".css") or (extension == ".opf") or (extension == ".ncx"):
						#changedFile = 
						changedFile = self.diffTheFiles(file.filename, otherfile.filename, otherEPUB, diff)
						if changedFile != None:
							changedFiles.append(changedFile)
				else: 
					continue
			if match == 0:
				print("This file only exists " + self.sourcepath + ": " + file.filename + "\n")
			#if 
		if len(changedFiles) > 0:
			#if len(changedFiles) == 1:
			#	changedFileNames = changedFiles[0]
			#if len(changedFiles) > 1:
			changedFileNames = ""
			for change in changedFiles:
				if changedFiles.index(change) == len(changedFiles):
					changedFileNames = changedFileNames + " " + change + "."
				else:
					changedFileNames = changedFileNames + " " + change + "\n"
			print("\n\nThe following files in the two EPUBs should be checked for differences:\n" + changedFileNames)
		elif len(changedFiles) == 0 and match == 1:
			print("\n\nOops, these files are identical in every way!")
		#elif match == 0:
		
	
	#def diffThoseEPUBs(self,
	
	sprintNumber = ""

if __name__ == "__main__":
	if len(sys.argv) == 1:
		print("add some EPUBs!")
	if len(sys.argv) == 2:
		print("add another EPUB!")
	if len(sys.argv) == 3:
		#print("let's do this thing")
		epub1 = Epub(sys.argv[1])
		epub2 = Epub(sys.argv[2])
		if (epub1.sourcepath != "NOPE") and (epub2.sourcepath != "NOPE"):
			#print("they're both EPUBs!!!")
			
			#Get all the files from each EPUB as ZipInfo objects.
			epub1Files = epub1.getPages()
			epub2Files = epub2.getPages()
			
			#Compare the files within each EPUB to each other.
			if len(epub1Files) > len(epub2Files):
				epub1.compareThoseEPUBs(epub2)
			elif len(epub1Files) < len(epub2Files):
				epub2.compareThoseEPUBs(epub1)
			else:
				epub1.compareThoseEPUBs(epub2)
				
				
		elif (epub1.sourcepath == "NOPE"):
			print(sys.argv[1] + " is not an EPUB")
		elif (epub2.sourcepath == "NOPE"):
			print(sys.argv[2] + " is not an EPUB")
		else:
			print("something went horribly wrong")
			
	if len(sys.argv) >= 4:
		print("too many arguments!")
else:
	print("being imported")
	