import sys,os,shutil,zipfile,time, os.path,re,subprocess,difflib,codecs #json,urllib2,urllib,
import xml.dom.minidom as minidom
from bs4 import BeautifulSoup

USAGE_MSG = "Usage: python ./newTextDiff.py PATH_TO_EPUB_1 PATH_TO_EPUB_2"

# needs to be global variable unfortunately to create new epub
opfPath = ''

globalDebug = False

def debug(msg):
	if globalDebug:
		print str(msg)

class Epub:

	#constructor does some basic error checking
	def __init__(self, sourcepath=None):
		self.logMsg = ""
		if sourcepath and os.path.exists(sourcepath) and os.path.isfile(sourcepath) and sourcepath.lower().endswith(".epub"):
			self.sourcepath = sourcepath
			# new model of folder processing
			self.myZipIn = zipfile.ZipFile(self.sourcepath, 'a')
			self.validationError = ""
			self.isbn = ""
		else:
			raise Exception("\n\nerror instantiating file " + sourcepath)

	#refactored print statements to log()
	def log(self, txt):
		self.logMsg = self.logMsg + formatLogText(str(txt)) + "\n"

	def getEpubVersion(self):
		opf = self.getOPF(self.myZipIn)
		dom = minidom.parseString(self.myZipIn.read(opf.orig_filename))
		thePackage = dom.getElementsByTagName('package')[0]
		versionNumber = thePackage.attributes['version'].value
		return versionNumber

	def isEpub_3(self):
		if self.getEpubVersion() == "3.0":
			return True
		else:
			return False

	def getNavTOC(self):
		return self.getNavNode("toc")
	def getNavLandmarks(self):
		return self.getNavNode("landmarks")
	def getNavPageList(self):
		return self.getNavNode("page-list")

	def getNavNode(self,aNode):
		myNav = self.getNav()
		dom = minidom.parseString(self.myZipIn.read(myNav.orig_filename))
		if len(dom.getElementsByTagName('nav')) > 0:
			navTags = dom.getElementsByTagName('nav')
			for aNavTag in navTags:
				if aNavTag.hasAttribute("epub:type"):
					if aNavTag.getAttribute("epub:type").lower() == aNode:
						return aNavTag
		else:
			raise Exception("\n\nno <nav> tags in nav ")
		self.log("\n\ncould not find " + aNode + " in nav")
		return None #raise Exception("\n\ncould not find " + aNode + " in nav")
	
	def getNav(self):
		if self.isEpub_3():
			opf = self.getOPF(self.myZipIn)
			dom = minidom.parseString(self.myZipIn.read(opf.orig_filename))
			theManifest = dom.getElementsByTagName('manifest')#[0]
			if len(theManifest) == 1:
				theManifest = theManifest[0]
				for item in theManifest.childNodes:
					if item.nodeType == 1: #ELEMENT_NODE
						if item.hasAttribute("properties"):
							if item.getAttribute("properties").lower() == "nav" and item.hasAttribute("href"):
								navPath = item.getAttribute("href")
			else:
				raise Exception("\n\nlength of manifest != 1 ")
			#debug("nav in opf is "+ navPath)
			for zipInfo in self.myZipIn.infolist():
				if zipInfo.filename == navPath:
					return zipInfo
			raise Exception("\n\nno nav found")#return None#"working on it"
		else:
			raise Exception("\n\nnot epub 3 , no nav ")#print "no nav"

	def getOPF(self,zipIn):
		global opfPath
		try:
			containerText = zipIn.read("META-INF/container.xml")
		except KeyError as k:
			raise k
		containerDom = minidom.parseString(containerText)
		theRootfile = containerDom.getElementsByTagName("rootfile")
		if len(theRootfile) == 1:
			if theRootfile[0].hasAttribute("full-path"):
				opfPath = theRootfile[0].getAttribute("full-path")
			else:
				self.log("STATUS: File Rejected.  Unable to find OPF")
		else:
			self.log("STATUS: File Rejected.  Unable to open OPF")
		for zipInfo in zipIn.infolist():
			if zipInfo.filename == opfPath:
				return zipInfo

	def isFixedPage(self):
		try:
			displayOptionsText = self.myZipIn.read("META-INF/com.apple.ibooks.display-options.xml")
			displayOptionsDom = minidom.parseString(displayOptionsText)
			options = displayOptionsDom.getElementsByTagName("option")
			#print len(options)
			for option in options:
				if option.hasAttribute("name"):
					if option.getAttribute("name") == "fixed-layout":
						if str(option.childNodes[0].nodeValue).lower() == "true":
							return True
		except KeyError, e:
			try:
				opf = self.getOPF(self.myZipIn)
				dom = minidom.parseString(self.myZipIn.read(opf.orig_filename))
				metadata = dom.getElementsByTagName("meta")
				for item in metadata:
					if item.hasAttribute("property"):
						if item.getAttribute("property") == "rendition:layout":
							if item.firstChild.nodeValue == "pre-paginated":
								return True
				return False
			except KeyError:
				self.log("STATUS: File Rejected.  Unable to determine format.")
				return False
		except Exception as e:
			self.log("STATUS: File Rejected. " + str(e))
			return False
		return False

	def getManifestDict(self):
		manifestDict = {}
		opf = self.getOPF(self.myZipIn)
		dom = minidom.parseString(self.myZipIn.read(opf.orig_filename))
		theManifest = dom.getElementsByTagName('manifest')#[0]
		if len(theManifest) == 1:		
			theManifest = theManifest[0]
			for item in theManifest.childNodes:
				if item.nodeType == 1: #ELEMENT_NODE
					if item.hasAttribute("id") and item.hasAttribute("href") and item.hasAttribute("media-type"):
						if item.getAttribute("media-type") == "application/xhtml+xml":
							id = item.getAttribute("id")
							href = item.getAttribute("href")
							manifestDict[id] = href
			#print manifestDict#media-type="application/xhtml+xml"
			return manifestDict
		else:
			raise Exception("\n\nlength of manifest != 1 ")

	def getHTMLContent(self, orig_filename):
		#print "\t"+orig_filename
		####global opfPath
		#print opfPath
		startPath = opfPath.split("/")[:-1]
		#for zipInfo in self.myZipIn.infolist():
		#	print zipInfo.orig_filename
		#sys.exit(-1)
		try:
			startPathString =  "".join(startPath)
			#print orig_filename
			theRealPath = os.path.join(startPathString,orig_filename)
			#print theRealPath
			#sys.exit(-1)
			data = self.myZipIn.read(theRealPath)
			#return data
		except Exception as e:
			print e
			return "xxx"
		return data

	def getSpineList(self):
		spineList = []
		opf = self.getOPF(self.myZipIn)
		dom = minidom.parseString(self.myZipIn.read(opf.orig_filename))
		theSpine = dom.getElementsByTagName('spine')#[0]
		if len(theSpine) == 1:
			theSpine = theSpine[0]
			for itemref in theSpine.childNodes:
				if itemref.nodeType == 1: #ELEMENT_NODE
					if itemref.hasAttribute("idref") and itemref.hasAttribute("linear"):  #check idref && linear
						linear = itemref.getAttribute("linear")
						if linear == "yes":
							idref = itemref.getAttribute("idref")
							spineList.append(idref)
			return spineList
		else:
			raise Exception("\n\nlength of spine != 1 ")

	def getOPF(self,zipIn):
		global opfPath
		try:
			containerText = zipIn.read("META-INF/container.xml")
		except KeyError as k:
			raise k
		containerDom = minidom.parseString(containerText)
		theRootfile = containerDom.getElementsByTagName("rootfile")
		if len(theRootfile) == 1:
			if theRootfile[0].hasAttribute("full-path"):
				opfPath = theRootfile[0].getAttribute("full-path")
				#return zipIn.read(opfPath)
			else:
				self.log("STATUS: File Rejected.  Unable to open OPF")
		else:
			self.log("STATUS: File Rejected.  Unable to open OPF")
		######
		for zipInfo in zipIn.infolist():
			if zipInfo.filename == opfPath:
				return zipInfo
		#return None
##### end class delcaration #####
def formatLogText(logText):
	localTime = time.strftime("%I:%M:%S %p")
	logText = "[" + localTime + " epub.py] " + logText
	return logText

def getShortName(fullPath):
	shortName = ""
	folders = fullPath.split('/')
	shortName = folders[-1]
	return shortName

def is_epub_file(path):
	if (not os.path.isdir(path) and path.endswith('.epub')):
		return True
	return False

def is_isbn_folder(path):
	if (os.path.isdir(path) and os.path.basename(path).startswith('978')):
		return True
	return False

def find_epub(folderpath):
	foundEpub = None
	#os.chdir(folderpath)
	for thisfile in os.listdir(folderpath):
		thispath = folderpath + '/' + thisfile
		#
		# must match *.epub and not be directory
		#
		if is_epub_file(thispath):
			return thispath
		elif os.path.isdir(thispath):
			if not foundEpub:
				foundEpub = find_epub(thispath)
	return foundEpub



# instantiate epub and returns True if the file is fixed page, False otherwise
def processInputFile(inputFilePath):
	debug("\tProcessing file " + inputFilePath)
	myEpub = None
	try:
		myEpub = Epub(inputFilePath)
		myEpub.log("Processing file " + inputFilePath)
		spineList = myEpub.getSpineList()
		manifestDict = myEpub.getManifestDict()
		debug("spine length: " + str( len(spineList) ))
		debug( spineList )
		debug(manifestDict)
		fullHTML = "<!DOCTYPE html>\n<html>\n<head></head>\n<body>\n"
		for itemref in spineList:
			debug(itemref)
			debug(manifestDict[itemref])
			data = myEpub.getHTMLContent(manifestDict[itemref])
			#debug(data)
			aFileBody = data[data.find("<body")+6 : data.rfind("</body")+0]
			#debug(aFileBody)
			fullHTML += aFileBody +"<hr/>"
			#debug("--------")
		#print("_______________________________")
		#print fullHTML
		return fullHTML
	except Exception as e:
		if myEpub:
			myEpub.log(e)
		else:
			debug("??? " +str(e))
	finally:
		endFileMessage = "----------End processing this file------------------\n"
		if myEpub:
			myEpub.log(endFileMessage)	
		else:
			debug(endFileMessage)

def	processInputFolder(inputFolderPath):
	debug("Processing folder " + inputFolderPath)
	return -1
	inputPaths  = []
	isFixedPage, notFixedPage, allOtherFailures = [] , [] , []
	
	for file in os.listdir(inputFolderPath):
		# gather list of items to process
		aFile =  os.path.join(inputFolderPath,file)
		if is_epub_file(aFile) or is_isbn_folder(aFile):
			inputPaths.append(aFile)
	debug(inputPaths)
	if len(inputPaths) > 0:
		for anItem in inputPaths:
			newDestination = None # declared early so available in scope of finally clause
			if is_epub_file(anItem):
				srcPath = anItem
			else:
				srcPath = find_epub(anItem)
			debug(srcPath)
			isFixed = False
			if srcPath:
				isFixed = processInputFile(srcPath)
				if isFixed:
					moveToSuccess(inputFolderPath, anItem)
					isFixedPage.append(anItem)
				else:
					notFixedPage.append(anItem)
			else: # i.e. an ISBN folder with no epub inside			
				print "NO EPUB IN ISBN FOLDER " + anItem
				allOtherFailures.append(anItem)
			debug("\t"+str(isFixed))

			#handleLogs
				
	#handle checksum
	checksum = len(inputPaths) - len(isFixedPage) - len(notFixedPage) - len(allOtherFailures)
	handleLogs(inputFolderPath,inputPaths,isFixedPage, notFixedPage,allOtherFailures )
	return checksum

def handleLogs(inputFolderPath,inputPaths,isFixedPage, notFixedPage,allOtherFailures ):
	logOutputPreface = "\n\t################ PreFlight Summary #######################\n"
	logOutputPreface += "Processed "+inputFolderPath+".\n"
	logOutputPreface += "\t"+str( len(inputPaths) ) +" input(s) found\n"
	logOutputPreface += "\t"+str( len(isFixedPage) ) +" fixed-page\n"
	logOutputPreface += "\t"+str( len(notFixedPage) ) +" reflowable\n"
	if len(allOtherFailures) > 0:
		logOutputPreface += "\t"+str( len(allOtherFailures) ) +" file/system errors\n"
	print logOutputPreface

def moveToSuccess(basePath, thingToMove):
	success_folder =  os.path.join( basePath,"z_Fixed_Page")
	if os.path.exists(success_folder):
		pass # "do not make folder"
	else:
		os.mkdir(success_folder)

	success_file =  os.path.join(success_folder, getShortName(thingToMove))
	os.rename(thingToMove , success_file )
	debug(success_file)

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print(USAGE_MSG)
	else: 
	   try:
		   epubText_1 = processInputFile(sys.argv[1])
		   epubText_2 = processInputFile(sys.argv[2])
		   #print(epubText_1)
		   soup_1 = BeautifulSoup( epubText_1 ,  "html.parser" )
		   soup_2 = BeautifulSoup( epubText_2 ,  "html.parser" )
		   htmldiff = difflib.HtmlDiff(wrapcolumn=40)
		   epub1_lines = soup_1.get_text().splitlines()
		   epub2_lines = soup_2.get_text().splitlines()
		   debug(len(epub1_lines))
		   debug(len(epub2_lines))
		   epub1_lines[:] = (value for value in epub1_lines if value != '')#epub1_lines.remove('')
		   epub2_lines[:] = (value for value in epub2_lines if value != '')#epub1_lines.remove('')
		   debug(len(epub1_lines))
		   debug(len(epub2_lines))		   
		   #print soup_2.get_text()
		   html_diff = htmldiff.make_file(epub1_lines, epub2_lines, fromdesc="first_one", todesc="second_one", context=True, numlines=0)
		   #HACK ; find better way to do this #html_diff = html_diff.replace('<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />','<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />')
		   html_diff = html_diff.replace('<meta http-equiv="Content-Type"','<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />')
		   html_diff = html_diff.replace('content="text/html; charset=ISO-8859-1" />','')
		   dir_path = os.path.dirname(sys.argv[1])
		   outputFilename = dir_path + "/diff_results.html"
		   outputSoupFile = codecs.open(outputFilename, "wb", "utf-8")
		   outputSoupFile.write(html_diff)
		   outputSoupFile.close()
	   except Exception as e:
		   print e
else:
	print "being imported"